package tech.quire.nrf52app;

import android.app.Application;
import android.util.Log;

import com.amplifyframework.AmplifyException;
import com.amplifyframework.api.aws.AWSApiPlugin;
import com.amplifyframework.api.graphql.model.ModelMutation;
import com.amplifyframework.auth.cognito.AWSCognitoAuthPlugin;
import com.amplifyframework.core.Amplify;
import com.amplifyframework.datastore.AWSDataStorePlugin;
import com.amplifyframework.datastore.generated.model.Device;
import com.amplifyframework.datastore.generated.model.RpiNrf52;

public class MyAmplifyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        try {
            Amplify.addPlugin(new AWSApiPlugin());
            Amplify.addPlugin(new AWSDataStorePlugin());
            Amplify.addPlugin(new AWSCognitoAuthPlugin());
            Amplify.configure(getApplicationContext());

            Amplify.Auth.signIn(
                    "test1",
                    "Test123456",
//                    "user1",
//                    "Passw0rd",
                    result -> {
                        if(!result.isSignInComplete()) {
                            Log.i("Nrf52App", "Sign in failed");
                            return;
                        }

                        Log.i("Nrf52App", "Sign in succeeded");


                    },
                    error -> Log.e("Nrf52App", error.toString())
            );

        } catch (AmplifyException e) {
            Log.e("Nrf52App", "Could not initialize Amplify", e);
        }
    }
}