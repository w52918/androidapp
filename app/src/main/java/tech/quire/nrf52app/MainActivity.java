package tech.quire.nrf52app;

//import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import com.amplifyframework.api.graphql.model.ModelMutation;
import com.amplifyframework.core.Amplify;
import com.amplifyframework.datastore.generated.model.Device;
import com.amplifyframework.datastore.generated.model.RpiNrf52;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;



import tech.quire.nrf52app.UartService;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
//import android.support.v4.content.LocalBroadcastManager;

import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements RadioGroup.OnCheckedChangeListener {
    private static final int REQUEST_SELECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int UART_PROFILE_READY = 10;
    public static final String TAG = "nRFUART";
    private static final int UART_PROFILE_CONNECTED = 20;
    private static final int UART_PROFILE_DISCONNECTED = 21;
    private static final int STATE_OFF = 10;

    TextView mRemoteRssiVal;
    RadioGroup mRg;
    private int mState = UART_PROFILE_DISCONNECTED;
    private UartService mService = null;
    private BluetoothDevice mDevice = null;
    private BluetoothAdapter mBtAdapter = null;
    private ListView messageListView;
    private ArrayAdapter<String> listAdapter;
    private Button btnConnectDisconnect,btnSend, btnSync;
    private EditText edtMessage;
    private Device device = null;
    private boolean sync_flag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        // Request location permission enable in the app
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
                Toast.makeText(this, "The permission to get BLE location data is required", Toast.LENGTH_SHORT).show();
            }else{
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {//wt no if before
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                }
            }
        }else{
            Toast.makeText(this, "Location permissions already granted", Toast.LENGTH_SHORT).show();
        }

        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBtAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        messageListView = (ListView) findViewById(R.id.listMessage);
        listAdapter = new ArrayAdapter<String>(this, R.layout.message_detail);
        messageListView.setAdapter(listAdapter);
        messageListView.setDivider(null);
        btnConnectDisconnect=(Button) findViewById(R.id.btn_select);
        btnSend=(Button) findViewById(R.id.sendButton);
        btnSync= findViewById(R.id.btn_sync);
        edtMessage = (EditText) findViewById(R.id.sendText);
        service_init();



        // Handle Disconnect & Connect button
        btnConnectDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mBtAdapter.isEnabled()) {
                    Log.i(TAG, "onClick - BT not enabled yet");
                    Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                }
                else {
                    if (btnConnectDisconnect.getText().equals("Connect")){

                        //Connect button pressed, open DeviceListActivity class, with popup windows that scan for devices

                        Intent newIntent = new Intent(MainActivity.this,  DeviceListActivity.class);
                        startActivityForResult(newIntent, REQUEST_SELECT_DEVICE);
                    } else {
                        //Disconnect button pressed
                        if (mDevice!=null)
                        {
                            mService.disconnect();

                        }
                    }
                }
            }
        });

        // Handle aws synchronize & unsynchronize button
        btnSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!sync_flag && btnConnectDisconnect.getText().equals("Disconnect")) { // Make sure the ble is connected first
                    Log.i(TAG, "onClick - BT not synced yet");
                    btnSync.setText("Un-sync");
                    sync_flag = true;
                }
                else {
                    btnSync.setText("Sync");
                    sync_flag = false;
                }
            }
        });

        // Handle Send button
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.sendText);
                String message = editText.getText().toString();
                byte[] value;
                try {
                    //send data to service
                    value = message.getBytes("UTF-8");
                    mService.writeRXCharacteristic(value);
                    //Update the log with time stamp
                    String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                    listAdapter.add("["+currentDateTimeString+"] TX: "+ message);
                    messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);
                    edtMessage.setText("");
                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        });

        // Set initial UI state
    }

    //UART service connected/disconnected
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            mService = ((UartService.LocalBinder) rawBinder).getService();
            Log.d(TAG, "onServiceConnected mService= " + mService);
            if (!mService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }

        }

        public void onServiceDisconnected(ComponentName classname) {
            ////     mService.disconnect(mDevice);
            mService = null;
        }
    };

    private Handler mHandler = new Handler() {
        @Override

        //Handler events that received from UART service
        public void handleMessage(Message msg) {

        }
    };

    private final BroadcastReceiver UARTStatusChangeReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            final Intent mIntent = intent;
            //*********************//
            if (action.equals(UartService.ACTION_GATT_CONNECTED)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                        Log.d(TAG, "UART_CONNECT_MSG");
                        btnConnectDisconnect.setText("Disconnect");
                        btnSync.setEnabled(true); //wt
                        edtMessage.setEnabled(true);
                        btnSend.setEnabled(true);
                        ((TextView) findViewById(R.id.deviceName)).setText(mDevice.getName()+ " - ready");
                        listAdapter.add("["+currentDateTimeString+"] Connected to: "+ mDevice.getName());
                        messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);
                        mState = UART_PROFILE_CONNECTED;
                    }
                });
            }

            //*********************//
            if (action.equals(UartService.ACTION_GATT_DISCONNECTED)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                        Log.d(TAG, "UART_DISCONNECT_MSG");
                        btnConnectDisconnect.setText("Connect");
                        btnSync.setText("Sync"); // wt
                        btnSync.setEnabled(false); //wt
                        //sync_flag = false; // wt
                        edtMessage.setEnabled(false);
                        btnSend.setEnabled(false);
                        ((TextView) findViewById(R.id.deviceName)).setText("Not Connected");
                        listAdapter.add("["+currentDateTimeString+"] Disconnected to: "+ mDevice.getName());
                        mState = UART_PROFILE_DISCONNECTED;
                        mService.close();
                        //setUiState();

                    }
                });
            }


            //*********************//
            if (action.equals(UartService.ACTION_GATT_SERVICES_DISCOVERED)) {
                mService.enableTXNotification();
            }
            //*********************//
            if (action.equals(UartService.ACTION_DATA_AVAILABLE)) {

                //"D3C278"
                final byte[] txValue = intent.getByteArrayExtra(UartService.EXTRA_DATA);
                runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            //String text = new String(txValue, "UTF-8");
                            int size = txValue.length;
                            int count = 0;
                            char[] hex2char= "0123456789ABCDEF".toCharArray();
                            char[] hexValue = new char[size*2];
                            for (byte n : txValue) { //n = 0x5E
                                hexValue[count++] = hex2char[(n & 0xF0) >> 4]; //5, ->5 + 48
                                hexValue[count++] = hex2char[(n & 0x0F)];
                            }
                            String text = new String(hexValue);
                            //System.out.println(text);
                            String currentDateTimeString = DateFormat.getTimeInstance().format(new Date());
                            listAdapter.add("["+currentDateTimeString+"] RX: "+text);
                            messageListView.smoothScrollToPosition(listAdapter.getCount() - 1);

                            //Log.i("User Input:", "hello wt!");

                            // Only parse the data for bonbouton board
                            if (size == 64) parse_data(txValue);
                            if (sync_flag) { // Only send data to aws when sync button is pressed
                                 // Create device item
                                if (device == null) {//only create once after connected
                                    device = Device.builder()
                                            //.sensorId("5f0df24bb939135429fdcc90")
                                            .sensorId(mDevice.getAddress())
                                            .name(mDevice.getName())
                                            .build();

                                    Amplify.API.mutate(ModelMutation.create(device),
                                            response -> {
                                                Log.i("Nrf52App", "Added device with id: " + response.getData().getId());
                                            },
                                            error -> {
                                                Log.e("Nrf52App", "Create device failed", error);
                                            });
                                }

                                RpiNrf52 data = RpiNrf52.builder()
                                        //.data("0888a1f3478bff0119fb1a0f19fb19f019f119e81170117011701170117011701170117011701170117011700800080008000800080001020102100207500001")
                                        .data(text)
                                        .sortHelper(0)
                                        .device(device)
                                        .build();
                                Amplify.API.mutate(ModelMutation.create(data),
                                        response -> {Log.i("Nrf52App", "Added data with id: " + response.getData().getId());},
                                        error -> {Log.e("Nrf52App", "Create data failed", error);});
                            }

                        } catch (Exception e) {
                            Log.e(TAG, e.toString());
                        }
                    }
                });
            }
            //*********************//
            if (action.equals(UartService.DEVICE_DOES_NOT_SUPPORT_UART)){
                showMessage("Device doesn't support UART. Disconnecting");
                mService.disconnect();
            }


        }
    };

    private void parse_data(byte sensor_data[]) { //parse the bonbouton sensor data
        int[] data = new int[sensor_data.length];
        for (int i = 0; i < sensor_data.length; i++) { //byte array to unsigned int array
            data[i] = sensor_data[i] & 0xFF;
        }

        TextView serial_num = findViewById(R.id.serial_number);
        serial_num.setText(String.valueOf(data[0]*256 + data[1])); //serial number


        TextView sample_time = findViewById(R.id.epoch_time); //epoch time
        long epoch_time = 0;
        for (int i = 2; i <= 5; i++) { //get epoch time
            epoch_time = epoch_time * 256 + data[i];
        }
        //System.out.println("serial number: " + epoch_time);
        Date date = new Date(epoch_time*1000);
        DateFormat format = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        //format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        String formatted = format.format(date);
        sample_time.setText(formatted);

        TextView battery = findViewById(R.id.battary_level);
        battery.setText(String.format("%.2f", (data[60]*256 + data[61]) * 0.001763)); //battery level

        TextView steps = findViewById(R.id.step_count);
        steps.setText(String.valueOf(data[62]*256 + data[63])); //step count

        TextView T0 = findViewById(R.id.temperature_0);
        T0.setText(String.format("%.2f", (data[8]*256 + data[9]) * 0.00390625)); //Temperature 0
        TextView T1 = findViewById(R.id.temperature_1);
        T1.setText(String.format("%.2f", (data[10]*256 + data[11]) * 0.00390625)); //Temperature 1
        TextView T2 = findViewById(R.id.temperature_2);
        T2.setText(String.format("%.2f", (data[12]*256 + data[13]) * 0.00390625)); //Temperature 2
        TextView T3 = findViewById(R.id.temperature_3);
        T3.setText(String.format("%.2f", (data[14]*256 + data[15]) * 0.00390625)); //Temperature 3
        TextView T4 = findViewById(R.id.temperature_4);
        T4.setText(String.format("%.2f", (data[16]*256 + data[17]) * 0.00390625)); //Temperature 4
        TextView T5 = findViewById(R.id.temperature_5);
        T5.setText(String.format("%.2f", (data[18]*256 + data[19]) * 0.00390625)); //Temperature 5

        TextView T6 = findViewById(R.id.temperature_6);
        T6.setText(String.format("%.2f", (data[20]*256 + data[21]) * 0.00390625)); //Temperature 6
        TextView T7 = findViewById(R.id.temperature_7);
        T7.setText(String.format("%.2f", (data[22]*256 + data[23]) * 0.00390625)); //Temperature 7
        TextView T8 = findViewById(R.id.temperature_8);
        T8.setText(String.format("%.2f", (data[24]*256 + data[25]) * 0.00390625)); //Temperature 8
        TextView T9 = findViewById(R.id.temperature_9);
        T9.setText(String.format("%.2f", (data[26]*256 + data[27]) * 0.00390625)); //Temperature 9
        TextView T10 = findViewById(R.id.temperature_10);
        T10.setText(String.format("%.2f", (data[28]*256 + data[29]) * 0.00390625)); //Temperature 10
        TextView T11 = findViewById(R.id.temperature_11);
        T11.setText(String.format("%.2f", (data[30]*256 + data[31]) * 0.00390625)); //Temperature 11

        TextView T12 = findViewById(R.id.temperature_12);
        T12.setText(String.format("%.2f", (data[32]*256 + data[33]) * 0.00390625)); //Temperature 12
        TextView T13 = findViewById(R.id.temperature_13);
        T13.setText(String.format("%.2f", (data[34]*256 + data[35]) * 0.00390625)); //Temperature 13
        TextView T14 = findViewById(R.id.temperature_14);
        T14.setText(String.format("%.2f", (data[36]*256 + data[37]) * 0.00390625)); //Temperature 14
        TextView T15 = findViewById(R.id.temperature_15);
        T15.setText(String.format("%.2f", (data[38]*256 + data[39]) * 0.00390625)); //Temperature 15
        TextView T16 = findViewById(R.id.temperature_16);
        T16.setText(String.format("%.2f", (data[40]*256 + data[41]) * 0.00390625)); //Temperature 16
        TextView T17 = findViewById(R.id.temperature_17);
        T17.setText(String.format("%.2f", (data[42]*256 + data[43]) * 0.00390625)); //Temperature 17

        TextView L1 = findViewById(R.id.load_sensor_1);
        L1.setText(String.format("%.2f", (data[44]*256 + data[45]) * 1.0)); //Load sensor 1
        TextView L2 = findViewById(R.id.load_sensor_2);
        L2.setText(String.format("%.2f", (data[46]*256 + data[47]) * 1.0)); //Load sensor 2
        TextView L3 = findViewById(R.id.load_sensor_3);
        L3.setText(String.format("%.2f", (data[48]*256 + data[49]) * 1.0)); //Load sensor 3
        TextView L4 = findViewById(R.id.load_sensor_4);
        L4.setText(String.format("%.2f", (data[50]*256 + data[51]) * 1.0)); //Load sensor 4
        TextView L5 = findViewById(R.id.load_sensor_5);
        L5.setText(String.format("%.2f", (data[52]*256 + data[53]) * 1.0)); //Load sensor 5

        TextView imu_x = findViewById(R.id.imu_x);
        int imu_val_x = data[54] * 256 + data[55];
        if (imu_val_x > 32767) imu_val_x = 32767 - imu_val_x;
        imu_x.setText(String.format("%.2f", imu_val_x * 0.0002241)); //IMU X

        TextView imu_y = findViewById(R.id.imu_y);
        int imu_val_y = data[56] * 256 + data[57];
        if (imu_val_y > 32767) imu_val_y = 32767 - imu_val_y;
        imu_y.setText(String.format("%.2f", imu_val_y * 0.0002241)); //IMU Y

        TextView imu_z = findViewById(R.id.imu_z);
        int imu_val_z = data[58] * 256 + data[59];
        if (imu_val_z > 32767) imu_val_z = 32767 - imu_val_z;
        imu_z.setText(String.format("%.2f", imu_val_z * 0.0002241)); //IMU Z
    }

    private void service_init() {
        Intent bindIntent = new Intent(this, UartService.class);
        bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

        LocalBroadcastManager.getInstance(this).registerReceiver(UARTStatusChangeReceiver, makeGattUpdateIntentFilter());
    }
    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(UartService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(UartService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(UartService.DEVICE_DOES_NOT_SUPPORT_UART);
        return intentFilter;
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");

        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(UARTStatusChangeReceiver);
        } catch (Exception ignore) {
            Log.e(TAG, ignore.toString());
        }
        unbindService(mServiceConnection);
        mService.stopSelf();
        mService= null;

    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        if (!mBtAdapter.isEnabled()) {
            Log.i(TAG, "onResume - BT not enabled yet");
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case REQUEST_SELECT_DEVICE:
                //When the DeviceListActivity return, with the selected device address
                if (resultCode == Activity.RESULT_OK && data != null) {
                    String deviceAddress = data.getStringExtra(BluetoothDevice.EXTRA_DEVICE);
                    mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(deviceAddress);

                    Log.d(TAG, "... onActivityResultdevice.address==" + mDevice + "mserviceValue" + mService);
                    ((TextView) findViewById(R.id.deviceName)).setText(mDevice.getName()+ " - connecting");
                    mService.connect(deviceAddress);


                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(this, "Bluetooth has turned on ", Toast.LENGTH_SHORT).show();

                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, "Problem in BT Turning ON ", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
            default:
                Log.e(TAG, "wrong request code");
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

    }


    private void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onBackPressed() {
        if (mState == UART_PROFILE_CONNECTED) {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
            showMessage("nRFUART's running in background.\n             Disconnect to exit");
        }
        else {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.popup_title)
                    .setMessage(R.string.popup_message)
                    .setPositiveButton(R.string.popup_yes, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton(R.string.popup_no, null)
                    .show();
        }
    }

}