package com.amplifyframework.datastore.generated.model;

import com.amplifyframework.core.model.annotations.HasMany;

import java.util.List;
import java.util.UUID;
import java.util.Objects;

import androidx.core.util.ObjectsCompat;

import com.amplifyframework.core.model.AuthStrategy;
import com.amplifyframework.core.model.Model;
import com.amplifyframework.core.model.ModelOperation;
import com.amplifyframework.core.model.annotations.AuthRule;
import com.amplifyframework.core.model.annotations.Index;
import com.amplifyframework.core.model.annotations.ModelConfig;
import com.amplifyframework.core.model.annotations.ModelField;
import com.amplifyframework.core.model.query.predicate.QueryField;

import static com.amplifyframework.core.model.query.predicate.QueryField.field;

/** This is an auto generated class representing the Device type in your schema. */
@SuppressWarnings("all")
@ModelConfig(pluralName = "Devices", authRules = {
  @AuthRule(allow = AuthStrategy.OWNER, ownerField = "owner", identityClaim = "cognito:username", operations = { ModelOperation.CREATE, ModelOperation.UPDATE, ModelOperation.DELETE, ModelOperation.READ }),
  @AuthRule(allow = AuthStrategy.GROUPS, groupClaim = "cognito:groups", groups = { "Admin" }, operations = { ModelOperation.CREATE, ModelOperation.UPDATE, ModelOperation.DELETE, ModelOperation.READ })
})
@Index(name = "ByName", fields = {"name","owner"})
@Index(name = "ByOwner", fields = {"owner","name"})
@Index(name = "BySensorId", fields = {"sensorId"})
public final class Device implements Model {
  public static final QueryField ID = field("id");
  public static final QueryField SENSOR_ID = field("sensorId");
  public static final QueryField NAME = field("name");
  public static final QueryField OWNER = field("owner");
  private final @ModelField(targetType="ID", isRequired = true) String id;
  private final @ModelField(targetType="String", isRequired = true) String sensorId;
  private final @ModelField(targetType="String", isRequired = true) String name;
  private final @ModelField(targetType="RpiNrf52") @HasMany(associatedWith = "device", type = RpiNrf52.class) List<RpiNrf52> rpiNrf52s = null;
  private final @ModelField(targetType="String") String owner;
  public String getId() {
      return id;
  }
  
  public String getSensorId() {
      return sensorId;
  }
  
  public String getName() {
      return name;
  }
  
  public List<RpiNrf52> getRpiNrf52s() {
      return rpiNrf52s;
  }
  
  public String getOwner() {
      return owner;
  }
  
  private Device(String id, String sensorId, String name, String owner) {
    this.id = id;
    this.sensorId = sensorId;
    this.name = name;
    this.owner = owner;
  }
  
  @Override
   public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      } else if(obj == null || getClass() != obj.getClass()) {
        return false;
      } else {
      Device device = (Device) obj;
      return ObjectsCompat.equals(getId(), device.getId()) &&
              ObjectsCompat.equals(getSensorId(), device.getSensorId()) &&
              ObjectsCompat.equals(getName(), device.getName()) &&
              ObjectsCompat.equals(getOwner(), device.getOwner());
      }
  }
  
  @Override
   public int hashCode() {
    return new StringBuilder()
      .append(getId())
      .append(getSensorId())
      .append(getName())
      .append(getOwner())
      .toString()
      .hashCode();
  }
  
  @Override
   public String toString() {
    return new StringBuilder()
      .append("Device {")
      .append("id=" + String.valueOf(getId()) + ", ")
      .append("sensorId=" + String.valueOf(getSensorId()) + ", ")
      .append("name=" + String.valueOf(getName()) + ", ")
      .append("owner=" + String.valueOf(getOwner()))
      .append("}")
      .toString();
  }
  
  public static SensorIdStep builder() {
      return new Builder();
  }
  
  /** 
   * WARNING: This method should not be used to build an instance of this object for a CREATE mutation.
   * This is a convenience method to return an instance of the object with only its ID populated
   * to be used in the context of a parameter in a delete mutation or referencing a foreign key
   * in a relationship.
   * @param id the id of the existing item this instance will represent
   * @return an instance of this model with only ID populated
   * @throws IllegalArgumentException Checks that ID is in the proper format
   */
  public static Device justId(String id) {
    try {
      UUID.fromString(id); // Check that ID is in the UUID format - if not an exception is thrown
    } catch (Exception exception) {
      throw new IllegalArgumentException(
              "Model IDs must be unique in the format of UUID. This method is for creating instances " +
              "of an existing object with only its ID field for sending as a mutation parameter. When " +
              "creating a new object, use the standard builder method and leave the ID field blank."
      );
    }
    return new Device(
      id,
      null,
      null,
      null
    );
  }
  
  public CopyOfBuilder copyOfBuilder() {
    return new CopyOfBuilder(id,
      sensorId,
      name,
      owner);
  }
  public interface SensorIdStep {
    NameStep sensorId(String sensorId);
  }
  

  public interface NameStep {
    BuildStep name(String name);
  }
  

  public interface BuildStep {
    Device build();
    BuildStep id(String id) throws IllegalArgumentException;
    BuildStep owner(String owner);
  }
  

  public static class Builder implements SensorIdStep, NameStep, BuildStep {
    private String id;
    private String sensorId;
    private String name;
    private String owner;
    @Override
     public Device build() {
        String id = this.id != null ? this.id : UUID.randomUUID().toString();
        
        return new Device(
          id,
          sensorId,
          name,
          owner);
    }
    
    @Override
     public NameStep sensorId(String sensorId) {
        Objects.requireNonNull(sensorId);
        this.sensorId = sensorId;
        return this;
    }
    
    @Override
     public BuildStep name(String name) {
        Objects.requireNonNull(name);
        this.name = name;
        return this;
    }
    
    @Override
     public BuildStep owner(String owner) {
        this.owner = owner;
        return this;
    }
    
    /** 
     * WARNING: Do not set ID when creating a new object. Leave this blank and one will be auto generated for you.
     * This should only be set when referring to an already existing object.
     * @param id id
     * @return Current Builder instance, for fluent method chaining
     * @throws IllegalArgumentException Checks that ID is in the proper format
     */
    public BuildStep id(String id) throws IllegalArgumentException {
        this.id = id;
        
        try {
            UUID.fromString(id); // Check that ID is in the UUID format - if not an exception is thrown
        } catch (Exception exception) {
          throw new IllegalArgumentException("Model IDs must be unique in the format of UUID.",
                    exception);
        }
        
        return this;
    }
  }
  

  public final class CopyOfBuilder extends Builder {
    private CopyOfBuilder(String id, String sensorId, String name, String owner) {
      super.id(id);
      super.sensorId(sensorId)
        .name(name)
        .owner(owner);
    }
    
    @Override
     public CopyOfBuilder sensorId(String sensorId) {
      return (CopyOfBuilder) super.sensorId(sensorId);
    }
    
    @Override
     public CopyOfBuilder name(String name) {
      return (CopyOfBuilder) super.name(name);
    }
    
    @Override
     public CopyOfBuilder owner(String owner) {
      return (CopyOfBuilder) super.owner(owner);
    }
  }
  
}
