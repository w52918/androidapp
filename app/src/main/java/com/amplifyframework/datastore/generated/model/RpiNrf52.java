package com.amplifyframework.datastore.generated.model;

import com.amplifyframework.core.model.annotations.BelongsTo;
import com.amplifyframework.core.model.temporal.Temporal;

import java.util.List;
import java.util.UUID;
import java.util.Objects;

import androidx.core.util.ObjectsCompat;

import com.amplifyframework.core.model.AuthStrategy;
import com.amplifyframework.core.model.Model;
import com.amplifyframework.core.model.ModelOperation;
import com.amplifyframework.core.model.annotations.AuthRule;
import com.amplifyframework.core.model.annotations.Index;
import com.amplifyframework.core.model.annotations.ModelConfig;
import com.amplifyframework.core.model.annotations.ModelField;
import com.amplifyframework.core.model.query.predicate.QueryField;

import static com.amplifyframework.core.model.query.predicate.QueryField.field;

/** This is an auto generated class representing the RpiNrf52 type in your schema. */
@SuppressWarnings("all")
@ModelConfig(pluralName = "RpiNrf52s", authRules = {
  @AuthRule(allow = AuthStrategy.OWNER, ownerField = "owner", identityClaim = "cognito:username", operations = { ModelOperation.CREATE, ModelOperation.UPDATE, ModelOperation.DELETE, ModelOperation.READ }),
  @AuthRule(allow = AuthStrategy.GROUPS, groupClaim = "cognito:groups", groups = { "Admin" }, operations = { ModelOperation.CREATE, ModelOperation.UPDATE, ModelOperation.DELETE, ModelOperation.READ })
})
@Index(name = "ByDeviceId", fields = {"deviceId","createdAt"})
@Index(name = "ByOwner", fields = {"owner","createdAt"})
@Index(name = "ByCreatedAt", fields = {"sortHelper","createdAt"})
public final class RpiNrf52 implements Model {
  public static final QueryField ID = field("id");
  public static final QueryField DEVICE = field("deviceId");
  public static final QueryField DATA = field("data");
  public static final QueryField CREATED_AT = field("createdAt");
  public static final QueryField OWNER = field("owner");
  public static final QueryField SORT_HELPER = field("sortHelper");
  private final @ModelField(targetType="ID", isRequired = true) String id;
  private final @ModelField(targetType="Device") @BelongsTo(targetName = "deviceId", type = Device.class) Device device;
  private final @ModelField(targetType="String", isRequired = true) String data;
  private final @ModelField(targetType="AWSDateTime") Temporal.DateTime createdAt;
  private final @ModelField(targetType="String") String owner;
  private final @ModelField(targetType="Int", isRequired = true) Integer sortHelper;
  public String getId() {
      return id;
  }
  
  public Device getDevice() {
      return device;
  }
  
  public String getData() {
      return data;
  }
  
  public Temporal.DateTime getCreatedAt() {
      return createdAt;
  }
  
  public String getOwner() {
      return owner;
  }
  
  public Integer getSortHelper() {
      return sortHelper;
  }
  
  private RpiNrf52(String id, Device device, String data, Temporal.DateTime createdAt, String owner, Integer sortHelper) {
    this.id = id;
    this.device = device;
    this.data = data;
    this.createdAt = createdAt;
    this.owner = owner;
    this.sortHelper = sortHelper;
  }
  
  @Override
   public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      } else if(obj == null || getClass() != obj.getClass()) {
        return false;
      } else {
      RpiNrf52 rpiNrf52 = (RpiNrf52) obj;
      return ObjectsCompat.equals(getId(), rpiNrf52.getId()) &&
              ObjectsCompat.equals(getDevice(), rpiNrf52.getDevice()) &&
              ObjectsCompat.equals(getData(), rpiNrf52.getData()) &&
              ObjectsCompat.equals(getCreatedAt(), rpiNrf52.getCreatedAt()) &&
              ObjectsCompat.equals(getOwner(), rpiNrf52.getOwner()) &&
              ObjectsCompat.equals(getSortHelper(), rpiNrf52.getSortHelper());
      }
  }
  
  @Override
   public int hashCode() {
    return new StringBuilder()
      .append(getId())
      .append(getDevice())
      .append(getData())
      .append(getCreatedAt())
      .append(getOwner())
      .append(getSortHelper())
      .toString()
      .hashCode();
  }
  
  @Override
   public String toString() {
    return new StringBuilder()
      .append("RpiNrf52 {")
      .append("id=" + String.valueOf(getId()) + ", ")
      .append("device=" + String.valueOf(getDevice()) + ", ")
      .append("data=" + String.valueOf(getData()) + ", ")
      .append("createdAt=" + String.valueOf(getCreatedAt()) + ", ")
      .append("owner=" + String.valueOf(getOwner()) + ", ")
      .append("sortHelper=" + String.valueOf(getSortHelper()))
      .append("}")
      .toString();
  }
  
  public static DataStep builder() {
      return new Builder();
  }
  
  /** 
   * WARNING: This method should not be used to build an instance of this object for a CREATE mutation.
   * This is a convenience method to return an instance of the object with only its ID populated
   * to be used in the context of a parameter in a delete mutation or referencing a foreign key
   * in a relationship.
   * @param id the id of the existing item this instance will represent
   * @return an instance of this model with only ID populated
   * @throws IllegalArgumentException Checks that ID is in the proper format
   */
  public static RpiNrf52 justId(String id) {
    try {
      UUID.fromString(id); // Check that ID is in the UUID format - if not an exception is thrown
    } catch (Exception exception) {
      throw new IllegalArgumentException(
              "Model IDs must be unique in the format of UUID. This method is for creating instances " +
              "of an existing object with only its ID field for sending as a mutation parameter. When " +
              "creating a new object, use the standard builder method and leave the ID field blank."
      );
    }
    return new RpiNrf52(
      id,
      null,
      null,
      null,
      null,
      null
    );
  }
  
  public CopyOfBuilder copyOfBuilder() {
    return new CopyOfBuilder(id,
      device,
      data,
      createdAt,
      owner,
      sortHelper);
  }
  public interface DataStep {
    SortHelperStep data(String data);
  }
  

  public interface SortHelperStep {
    BuildStep sortHelper(Integer sortHelper);
  }
  

  public interface BuildStep {
    RpiNrf52 build();
    BuildStep id(String id) throws IllegalArgumentException;
    BuildStep device(Device device);
    BuildStep createdAt(Temporal.DateTime createdAt);
    BuildStep owner(String owner);
  }
  

  public static class Builder implements DataStep, SortHelperStep, BuildStep {
    private String id;
    private String data;
    private Integer sortHelper;
    private Device device;
    private Temporal.DateTime createdAt;
    private String owner;
    @Override
     public RpiNrf52 build() {
        String id = this.id != null ? this.id : UUID.randomUUID().toString();
        
        return new RpiNrf52(
          id,
          device,
          data,
          createdAt,
          owner,
          sortHelper);
    }
    
    @Override
     public SortHelperStep data(String data) {
        Objects.requireNonNull(data);
        this.data = data;
        return this;
    }
    
    @Override
     public BuildStep sortHelper(Integer sortHelper) {
        Objects.requireNonNull(sortHelper);
        this.sortHelper = sortHelper;
        return this;
    }
    
    @Override
     public BuildStep device(Device device) {
        this.device = device;
        return this;
    }
    
    @Override
     public BuildStep createdAt(Temporal.DateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }
    
    @Override
     public BuildStep owner(String owner) {
        this.owner = owner;
        return this;
    }
    
    /** 
     * WARNING: Do not set ID when creating a new object. Leave this blank and one will be auto generated for you.
     * This should only be set when referring to an already existing object.
     * @param id id
     * @return Current Builder instance, for fluent method chaining
     * @throws IllegalArgumentException Checks that ID is in the proper format
     */
    public BuildStep id(String id) throws IllegalArgumentException {
        this.id = id;
        
        try {
            UUID.fromString(id); // Check that ID is in the UUID format - if not an exception is thrown
        } catch (Exception exception) {
          throw new IllegalArgumentException("Model IDs must be unique in the format of UUID.",
                    exception);
        }
        
        return this;
    }
  }
  

  public final class CopyOfBuilder extends Builder {
    private CopyOfBuilder(String id, Device device, String data, Temporal.DateTime createdAt, String owner, Integer sortHelper) {
      super.id(id);
      super.data(data)
        .sortHelper(sortHelper)
        .device(device)
        .createdAt(createdAt)
        .owner(owner);
    }
    
    @Override
     public CopyOfBuilder data(String data) {
      return (CopyOfBuilder) super.data(data);
    }
    
    @Override
     public CopyOfBuilder sortHelper(Integer sortHelper) {
      return (CopyOfBuilder) super.sortHelper(sortHelper);
    }
    
    @Override
     public CopyOfBuilder device(Device device) {
      return (CopyOfBuilder) super.device(device);
    }
    
    @Override
     public CopyOfBuilder createdAt(Temporal.DateTime createdAt) {
      return (CopyOfBuilder) super.createdAt(createdAt);
    }
    
    @Override
     public CopyOfBuilder owner(String owner) {
      return (CopyOfBuilder) super.owner(owner);
    }
  }
  
}
