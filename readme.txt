1 Firmware
The firmware uses bonbouton boards. The frequency of the data may vary from 1 Sec to 10 min based on the setting in u_config.h file.
Nothing specially needed to be done on the firmware side. It’s all fixed.

2 Android App
The app is called Nrf52App. Please use android studio to open and run the code. For future development, the following steps need to set up:
•	Have the amplify and cli installed on your laptop. The link is : https://docs.amplify.aws/lib/q/platform/android
If you are using an existing amplify backend with web app or other applications. Be sure first synchronize the backend by using
amplify pull
To do this, you should first navigate to the root of your android project directory. Then, replace the file team-provider-info.json in ../amplify/ folder.

•	Have the phone connected to the wifi.
•	For more info about the app, please check the bookmark android folder

3 AWS cloud
EC2 is used to host the webapp. Appsync is used to connect the webapp and android to the dynamodb. Cognito is used for user log in. 

4 Python analysis
Python directly connected to the dynamodb. So, the way it queries data is different from android and webapp. A sample code is written and stored in C:/git/aws_dynamodb

5 Raspberry Pi
A python code named nrf52_real is used to connect the bonbouton board and dynamodb. It pushes data directly into the database.
But, because the ble adapter we used is based on old ble protocol, it’s not able to receive all the 64 bytes data. Instead, only 20 bytes is received.
